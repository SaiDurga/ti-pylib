import utils as u
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns


def main():
    sns.set(style='darkgrid')
    x = np.random.normal(size=100)
    u.prob_hist(x)
    plt.show()


main()
