import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from biokit.viz import corrplot
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import GridSearchCV
import scipy.stats as s


def col_thresh(df, thresh):
    '''
    input: dataset, threshold(number)
    return: column names whose no of unique vales are less than the threshold value given
    '''
    result = []
    for i in range(len(df.columns)):
        num = df[df.columns[i]].nunique()
        if num <= thresh:
            result.append(df.columns[i])
    return result

# Print 3 different samples of a data
def samples(data):
    head = data.head()
    tail = data.tail()
    var = int(len(data)/2) - 2
    var1 = int(len(data)/2) + 2
    middle = data.iloc[var:var1]
    return head,tail,middle


# function to get scatter plot between variables for given figure size
def scatter_plot(x, y, data, fig_size=(8,6)):
    '''
    x, y    : string, series. If strings, these should correspond with column names in data.
    data    : DataFrame
    fig_size : tuple of integers, width, height in inches.
    '''
    plt.figure(figsize = fig_size)
    sns.regplot(x, y, data=data, fit_reg=False)

# function to split dataset into train and test datasets
def tt_split(features, label, test_size1=0.1, random_state1=42):
    '''
    input   : features and label
    Return : features train set, features test set, label train set, label test set
    '''
    fea_train, fea_test, lab_train, lab_test = train_test_split(features, label,
                                                                test_size= test_size1,
                                                                random_state=random_state1)
    return fea_train, fea_test, lab_train, lab_test

# function to draw correlation plot
def corr_plot(features, fig_size=(10,8)):
    '''
    input    : features
    fig_size : tuple of integers, width, height in inches.
    '''
    correlation = abs(features.corr())
    c = corrplot.Corrplot(correlation)
    _ , ax = plt.subplots(figsize=fig_size)
    c.plot(method='circle', cmap=('white', 'blue'), ax= ax, fontsize=10)

# function to display results
def res_display(*args):
    '''
    input :  Takes n number of Tuple as arguments,
             where each Tuple takes (Model name, RMS Error, R Square Error, Execution Time) as values
    '''
    result = '{:^20}|{:^15}|{:^15}|{:^16}\n'.format('Model Name', 'RMS Error',
                                                    'R2 Score', 'Execution Time')
    result = result+ '--------------------------------------------------------------------------\n'
    for i1,i2,i3,i4 in args:
        result = result+'{:^20}|{:15.3f}|{:15.3f}|{:16.3f}\n'.format(i1,i2,i3,i4)
    return result

# function to check zero nulls in the data set
def is_zero_nulls(features):
    '''
    input  : fetures(Data set)
    return : Boolean (True if zero nulls in dataset, else false)
    '''
    return ~features.isnull().any().any()

#function to find cross val mean and cross val std
def crossval(reg, data_transformed, labels, scoring='neg_mean_squared_error', cv=10):
    '''
    input: regression model object, transformed data(pipelined), labels
    return: cross validation mean and cross validation standard deviation
    '''
    scores = cross_val_score(reg, data_transformed, labels, scoring, cv)
    sq_scores = np.sqrt(-scores)
    cross_val_mean = sq_scores.mean()
    cross_val_std = sq_scores.std()
    return cross_val_mean, cross_val_std

# function to get feature importances in sorted order
def fea_imp(grid_search, fea_train):
    '''
    input: gridsearch object, features(dataset)
    return: feature importances in sorted order
    '''
    imps = grid_search.best_estimator_.feature_importances_
    return sorted(zip(imps, range(len(fea_train))), reverse = True)

#function to get performance measures of classifier
def performance_clf(clf, X_train, y_train, cv=3):
    '''
    input: classifer object, features(training dataset), labels(training dataset)
    return: confusion matrix, precision score, recall score, f1 score
    '''
    crossvalpredictions = cross_val_predict(clf, X_train, y_train, cv)
    con_mat = confusion_matrix(y_train, crossvalpredictions)
    pre_sco = precision_score(y_train, crossvalpredictions)
    rec_sco = recall_score(y_train, crossvalpredictions)
    f1_sco = f1_score(y_train, crossvalpredictions)
    return con_mat, pre_sco, rec_sco, f1_sco

#function to find percentages of a column with respect to another column
def perc_wrt_another_column(df, group_col, col):
    '''
    input: dataset, groupby columnn(based on which grouping is done), col(for which % is to be calculated)
    return: dataframe(with percentages of column)
    '''
    a = (df.groupby(group_col)[col].sum()/df.groupby(group_col).size())*100
    a.name = 'values'
    a1 = a.reset_index()
    return a1


# function to find the correlation between the columns
def correlation_matrix(data,column_name,ascending1 = False):
    '''
    input : dataset,column_name(which we want to corelate other columns),ascending(to sort the values)
    return : correlation matrix
    '''
    matrix = np.abs(data.corr())
    corr_matrix = matrix[column_name].sort_values(ascending=ascending1)
    return corr_matrix

# function to find the skewness of the each column in the data
def top_skew(data):
    '''
    input : dataset
    return : skewness of each column in a sorted order and the order is split into two lists based on the value given by us.
    '''
    res = []
    res1 = []
    skew = data.skew()
    sort = skew.sort_values(ascending = True)
    for i in range(len(sort)):
        if (sort[i] >= 0):
            res.append(sort[i])
        else:
            res1.append(sort[i])
    return res, res1

# function to tune the best model using gridsearch
def gridsearch(reg,param_grid,features,labels,cv=3,scoring ='neg_mean_squared_error'):
    '''
    input : regression model,features,labels,param_grid(contains list pf estimators and features),cv and score
    return : best estimator and best parameters
    '''
    grid = GridSearchCV(reg, param_grid, cv=3, scoring=scoring,n_jobs=4)
    x = grid.fit(features,labels)
    return grid.best_params_,grid.best_estimator_


def prob_hist(x):
    """Show Probability and Histogram plots of a random variable side-by-side
    to show assess the skewness.

    Parameters:
    x(pd.Series): values of a random variable
    """
    fig, (ax1, ax2) = plt.subplots(ncols=2)
    s.probplot(x, plot=ax1)
    sns.distplot(x, ax=ax2)
